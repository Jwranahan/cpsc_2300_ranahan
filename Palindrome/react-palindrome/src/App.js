import logo from "./logo.svg";
import "./App.css";
import React, { useState } from "react";

function App() {
    const [inputText, setInputText] = useState("");
    const [palindrome, setPalindrome] = useState("");

    function checkPalindrome() {
        const reverse = inputText.split("").reverse().join("");
        if (inputText === reverse) {
            setPalindrome("It is a palindrome");
        } else {
            setPalindrome("It is not a palindrome");
        }
    }

    return (
        <div className="App">
            <header>
                <input id="input" onChange={(e) => setInputText(e.target.value)} placeholder="Enter your string" />
                <div>
                    <button onClick={checkPalindrome}>Is it a palindrome?</button>
                </div>
                <div>Is it?: {palindrome}</div>
            </header>
        </div>
    );
}

export default App;
