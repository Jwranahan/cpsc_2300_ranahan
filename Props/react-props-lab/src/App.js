import "./App.css";
import React, { useState } from "react";
import Search from "./Components/Search";
import List from "./Components/List";

/**
 * Will use 2 components, Search and List, to form a list of results from a search
 * @returns returns a list generated from the search result of the search component
 */
function App() {
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <div className="App">
            <header className="App-header">
                <Search setSearchTerm={setSearchTerm} />
                <List searchTerm={searchTerm} />
            </header>
        </div>
    );
}

export default App;
