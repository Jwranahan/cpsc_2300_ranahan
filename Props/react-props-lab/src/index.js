import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { themeProvider } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";

import { red, white } from "@material-ui/core/colors";

const theme = createMuiTheme({
    pallete: {
        primary: {
            main: red[700]
        },
        secondary: {
            main: white[700]
        }
    }
});

ReactDOM.render(
    <React.StrictMode>
        <themeProvider Theme={theme}>
            <App />
        </themeProvider>
    </React.StrictMode>,
    document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
