/**
 * Will take the search term and filter out any non-matching results from the list
 * @param {Search term} props 
 * @returns the filtered list
 */

function List(props) {
    //The list of searchable terms
    const sports = [
        "football",
        "baseball",
        "soccer",
        "billards",
        "tennis",
        "basketball",
        "polo",
        "waterpolo",
        "swimming"
    ];

    return (
        <div>
            {sports
                .filter((sport) => sport.includes(props.searchTerm))
                .map((sport) => (
                    <div>{sport}</div>
                ))}
        </div>
    );
}

export default List;
