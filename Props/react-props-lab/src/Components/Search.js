/**
 * Will take in input to set search term
 * @param {setSearchTerm} props 
 * @returns searchterm
 */

function Search(props) {
    return <input placeholder="Search" onChange={(e) => props.setSearchTerm(e.target.value)}></input>;
}

export default Search;
