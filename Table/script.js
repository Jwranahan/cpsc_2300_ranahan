"use strict";

// TLDR Dont use Var
var myVariable = "Jacob";
console.log(myVariable);

// let
let myInt = 5;
let myString = "Blah";
let myObject = {};
let myArray = [1, "hmm", {}, []];

// Compares value
// ==
console.log("5" == 5);

//Compares value and type
// ===
console.log("5" === 5);

// const
const myValue = 5;


// functions in javascript
function myFunction(param1, param2) {
    if (typeof param1 !== "string") {
        alert("Wrong param type!")
    }

    return `${param1} ${param2}`;
}

console.log(myFunction("Jacob", "Ranahan"));

// es6 arrow/function expression