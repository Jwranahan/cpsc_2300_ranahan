"use strict";

const tableBody = document.getElementById("tableBody");
const sizeInput = document.getElementById("sizeInput");
const toppingsInput = document.getElementById("toppingsInput");
const numberOfPizzasInput = document.getElementById("numberOfPizzas");
const addButton = document.getElementById("addButton");

sizeInput.addEventListener("input", validateInputs);
numberOfPizzasInput.addEventListener("input", validateInputs);
toppingsInput.addEventListener("input", validateInputs);

// registering the click event to the add button - calling the addRecord function
addButton.addEventListener("click", addRecord);

//array of pizza orders
let orderArray = [];

/**
 * Verifies that inputs both have values
 * If so, enables (removes the disabled attr) the add button,
 * otherwise, it disables the button
 */
function validateInputs() {
    if (sizeInput.value && numberOfPizzasInput.value && toppingsInput.value) {
        addButton.removeAttribute("disabled");
    } else {
        addButton.setAttribute("disabled", "");
    }
}

/**
 * appends record to the array, then calls the renderTableBody function
 */
function addRecord() {
    orderArray.push({
        size: sizeInput.value,
        numberOfPizzas: numberOfPizzasInput.value,
        toppings: toppingsInput.value
    });

    localStorage.setItem("orders", JSON.stringify(orderArray)); //Set item to local storage here
    let storage = localStorage.getItem("orders");
    let ordersArray = JSON.parse(storage);

    renderTableBody(ordersArray); //Added ordersArray to renderTableBody for easy page loading
    sizeInput.value = "";
    numberOfPizzasInput.value = "";
    toppingsInput.value = "";

    validateInputs();
}

function loadFromStorage() {
    const storage = localStorage.getItem("orders");
    console.log(storage);
    if (storage) {
        try {
            let ordersArray = JSON.parse(storage);
            renderTableBody(ordersArray); //Will load storafe if there is anything to load
        } catch (e) {
            console.log(e);
        }
    }
}

/**
 * Adds a record to the body of the table
 * REQUIRES name and description inputs to have a value
 * Clears out inputs after success
 */
function renderTableBody(ordersArray) {
    //clear out the table before we add all of the records to it
    tableBody.innerHTML = "";
    ordersArray.forEach((order, index) => {
        const tableRow = tableBody.insertRow(-1);
        const iconCell = tableRow.insertCell(0);
        const sizeCell = tableRow.insertCell(1);
        const toppingsCell = tableRow.insertCell(2);
        const numberOfPizzasCell = tableRow.insertCell(3);
        const actionCell = tableRow.insertCell(4);
        actionCell.className = "px-6 py-4 whitespace-nowrap text-sm text-gray-500";
        iconCell.className = "px-6 py-4 whitespace-nowrap text-sm text-gray-500";
        sizeCell.className = "px-6 py-4 whitespace-nowrap text-sm text-gray-500";
        toppingsCell.className = "px-6 py-4 whitespace-nowrap text-sm text-gray-500";
        numberOfPizzasCell.className = "px-6 py-4 whitespace-nowrap text-sm text-gray-500";

        sizeCell.innerText = order.size;
        toppingsCell.innerText = order.toppings;
        numberOfPizzasCell.innerText = order.numberOfPizzas;
        iconCell.innerHTML = `<i class="fas fa-pizza-slice"></i>`;

        actionCell.innerHTML = `<i class="fas fa-minus-circle icon-button delete-button" onclick="deleteOrder(${index})"></i>`;
    });
}

/**
 * splices the array by index
 * @param {integer} - index for array
 */
function deleteOrder(index) {
    //retrieve 1 (hopefully) record - returning an array
    if (confirm("Are you sure you want to delete this order?")) {
        orderArray.splice(index, 1);
        localStorage.setItem("orders", JSON.stringify(orderArray));
        //Instead of deleting an item from the local storage, I reset it without the removed value
        const storage = localStorage.getItem("orders");
        let ordersArray = JSON.parse(storage); //Even if the cancellation is cancelled, it will still reload the array

        renderTableBody(ordersArray);
    }
}

loadFromStorage();
