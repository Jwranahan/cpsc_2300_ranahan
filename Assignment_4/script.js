"use strict";

let heads = 0; //Heads win count
let tails = 0; //Tails win count
let chance = 50; //Chance for flip out of 100, higher number means there is a greater chance for heads
let winFlip = ""; //Last winning flip
let headTailsArray = []; //Storage for the flip

/**
 * Will check to see if fairness has a value and will set it to input
 */
function makeItFair() {
    let fair = document.getElementById("fairness").value;
    if (fair != "") {
        chance = fair;
        document.getElementById("fairness").value = "";
    }
}

/**
 * Will flip the coin, chance the picture to the coin flipped,
 * and call addRecords to store the data
 */
function coinFlip() {
    let myInt = Math.random() * 100; //Generates a number between 1-100
    let fileName;
    //If the number generated is less than the chance, the flip is heads
    if (myInt <= chance) {
        document.getElementById("result").innerHTML = "Result: Heads";
        fileName = "Heads.jpg";
        heads += 1; //Adds one to the heads win counter
        winFlip = "Heads";
        document.getElementById("coin").innerHTML =
            "Heads: " + "<strong>" + heads + "</strong>" + "    Tails: " + "<strong>" + tails + "</strong>";
    } else {
        document.getElementById("result").innerHTML = "Result: Tails";
        fileName = "Tails.jpg";
        tails += 1; //Adds one to the tails win counter
        winFlip = "Tails";
        document.getElementById("coin").innerHTML =
            "Heads: " + "<strong>" + heads + "</strong>" + "    Tails: " + "<strong>" + tails + "</strong>";
    }
    addRecords(); //Calls function to add record to table and local storage
    const coinResult = document.getElementById("coinFlipped");
    coinResult.src = fileName;
}

/**
 * Will add heads, tails, flipChance, and winner to an array and put it into local storage 
 */
function addRecords() {
    headTailsArray.push({
        head: heads,
        tail: tails,
        flipChance: chance,
        winner: winFlip
    });
    console.log(headTailsArray);
    localStorage.setItem("headTails", JSON.stringify(headTailsArray)); //Set item to local storage here
    let storage = localStorage.getItem("headTails");
    let Array = JSON.parse(storage); //Parse all local storage for table construction
    renderTable(Array); //Passes local storage to the renderTable function
}

/**
 * Resets the score so that if new records are added they will be fresh
 * DOES NOT EFFECT LOCAL STORAGE
 */
function clearScore() {
    //Resets to default values if confirmed
    if (confirm("Are you sure you want to clear the score and reset the chance?")) {
        heads = 0;
        tails = 0;
        chance = 50;
        winFlip = "";
        document.getElementById("result").innerHTML = "Please flip the coin";
        document.getElementById("fairness").value = "";
        document.getElementById("coin").innerHTML =
            //`Heads + heads + "</strong>" + "    Tails: " + "<strong>" + tails + "</strong>`;
    }
}

/**
 * Will load the storage up (if there is any) and pass it to be rendered in the renderTable function
 */
function loadTableLog() {
    const storage = localStorage.getItem("headTails");
    if (storage) {
        try {
            let Array = JSON.parse(storage);
            headTailsArray = Array;
            renderTable(Array); //Will load storage if there is anything to load
        } catch (e) {
            console.log(e);
        }
    }
}

/**
 * Will render the table with local data
 * @param Array1 - Arrays that will be added to the table
 */
function renderTable(Array1) {
    tableBody.innerHTML = "";

    //Uses a forEach on the local storage array passed to it and 
    //fills the table with all the needed records
    Array1.forEach((flip, index) => {
        const tableRow = tableBody.insertRow(-1);
        const headCell = tableRow.insertCell(0);
        const tailCell = tableRow.insertCell(1);
        const chanceCell = tableRow.insertCell(2);
        const winCell = tableRow.insertCell(3);
        const actionCell = tableRow.insertCell(4);
        //Fills each new cell with appropriate data
        headCell.innerText = flip.head;
        tailCell.innerText = flip.tail;
        chanceCell.innerText = flip.flipChance;
        winCell.innerText = flip.winner;
        //Button will have an index tied to it that will allow the user to delete that specific record
        actionCell.innerHTML = `<button onclick="deleteRecord(${index})">Delete Flip</button>`;
    });
}

/**
 * Will remove a record from local storage and reload the table
 * @param index
 */
function deleteRecord(index) {
    //After confirmation, record is removed
    if (confirm("Are you sure you want to delete this record?")) {
        headTailsArray.splice(index, 1);
        localStorage.setItem("headTails", JSON.stringify(headTailsArray));

        //Instead of deleting an item from the local storage, I reset it without the removed value
        const storage = localStorage.getItem("headTails");

        //Even if the cancellation is cancelled, it will still reload the array
        let Array1 = JSON.parse(storage); 
        renderTable(Array1);
    }
}

//Loads any local data upon launching page
loadTableLog();
