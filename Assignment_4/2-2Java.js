"use strict";


let heads = 0;
let tails = 0;
let chance = 50;
let winFlip = "";
let headTailsArray = [];

function makeItFair() {
    let fair = document.getElementById("fairness").value;
    if (fair != "") {
        chance = fair;
        document.getElementById("fairness").value = "";
    }
}

function coinFlip() {
    let myInt = Math.random() * 100;
    let fileName;
    if (myInt <= chance) {
        document.getElementById("result").innerHTML = "Result: Heads";
        fileName = "Heads.jpg";
        heads += 1;
        winFlip = "Heads";
        document.getElementById("coin").innerHTML =
            "Heads: " + "<strong>" + heads + "</strong>" + "    Tails: " + "<strong>" + tails + "</strong>";
    } else {
        document.getElementById("result").innerHTML = "Result: Tails";
        fileName = "Tails.jpg";
        tails += 1;
        winFlip = "Tails";
        document.getElementById("coin").innerHTML =
            "Heads: " + "<strong>" + heads + "</strong>" + "    Tails: " + "<strong>" + tails + "</strong>";
    }
    addRecords();
    const coinResult = document.getElementById("coinFlipped");
    coinResult.src = fileName;
}

function addRecords() {
    headTailsArray.push({
        head: heads,
        tail: tails,
        flipChance: chance,
        winner: winFlip
    });
    console.log(headTailsArray);
    localStorage.setItem("headTails", JSON.stringify(headTailsArray)); //Set item to local storage here
    let storage = localStorage.getItem("headTails");
    let Array = JSON.parse(storage);
    renderTable(Array);
}

function clearScore() {
    if (confirm("Are you sure you want to clear the score and reset the chance?")) {
        heads = 0;
        tails = 0;
        chance = 50;
        winFlip = "";
        document.getElementById("result").innerHTML = "Please flip the coin";
        document.getElementById("fairness").value = "";
        document.getElementById("coin").innerHTML =
            "Heads: " + "<strong>" + heads + "</strong>" + "    Tails: " + "<strong>" + tails + "</strong>";
    }
}

function loadTableLog() {
    const storage = localStorage.getItem("headTails");
    if (storage) {
        try {
            let Array = JSON.parse(storage);
            headTailsArray = Array;
            renderTable(Array); //Will load storafe if there is anything to load
        } catch (e) {
            console.log(e);
        }
    }
}

function renderTable(Array1) {
    tableBody.innerHTML = "";
    Array1.forEach((flip, index) => {
        const tableRow = tableBody.insertRow(-1);
        const headCell = tableRow.insertCell(0);
        const tailCell = tableRow.insertCell(1);
        const chanceCell = tableRow.insertCell(2);
        const winCell = tableRow.insertCell(3);
        const actionCell = tableRow.insertCell(4);
        headCell.innerText = flip.head;
        tailCell.innerText = flip.tail;
        chanceCell.innerText = flip.flipChance;
        winCell.innerText = flip.winner;
        actionCell.innerHTML = `<button onclick="deleteRecord(${index})">Delete Flip</button>`;
    });
}

function deleteRecord(index) {
    if (confirm("Are you sure you want to delete this record?")) {
        headTailsArray.splice(index, 1);
        localStorage.setItem("headTails", JSON.stringify(headTailsArray));
        //Instead of deleting an item from the local storage, I reset it without the removed value
        const storage = localStorage.getItem("headTails");
        let Array1 = JSON.parse(storage); //Even if the cancellation is cancelled, it will still reload the array

        renderTable(Array1);
    }
}

loadTableLog();
