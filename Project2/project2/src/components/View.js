import React, { useEffect, useState } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
    tableStyle: {
        marginLeft: "auto",
        marginRight: "auto",
        width: "80%"
    },
    Media: {
        maxHeight: 225,
        width: "auto"
    }
}));

/**
 * Takes in the pokemon id and uses it to display a full table of data for that pokemon
 * @param {setCurrentComponent, currentPokemon} props 
 * @returns A table of the requested pokemon
 */
function View(props) {
    const [pokemon, setPokemon] = useState([]);
    const classes = useStyles(); //Styling for the buttons and table

    const pokemonUri = "https://web-app-pokemon.herokuapp.com"; //The base pokemon link

    /**
     * When called, will send a get request to the pokemonUri and will return back a pokemon matching the id passed
     */
    async function viewPokemon() {
        const id = props.currentPokemon;
        try {
            const response = await axios({
                method: "get", //will get the list of one pokemon
                url: `${pokemonUri}/pokemon/${id}`, //View pokemon based on id passed
                headers: {
                    "User-Id": "Jacob-R"
                }
            });
            setPokemon(response.data); //Get the information of the selected pokemon
            console.log(pokemon);
        } catch (e) {
            //some error occurred
        }
    }

    useEffect(() => {
        viewPokemon();
    }, []); //Makes sure an infinite loop doesn't happen

    /**
     * When called will change the component view back to the list view
     */
    function listView() {
        props.setCurrentComponent("List"); 
    }

    /**
     * Creates a whole table with the specified pokemon data grabbed from the axios request
     */
    return (
        <div>
            <TableContainer className={classes.tableStyle}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Pokemon</TableCell>
                            <TableCell align="center">Type</TableCell>
                            <TableCell align="center">Description</TableCell>
                            <TableCell align="center">Pokemon Picture</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            <TableRow key={pokemon.id}>
                                <TableCell align="center">{pokemon.name}</TableCell>
                                <TableCell align="center">{pokemon.type}</TableCell>
                                <TableCell align="center">{pokemon.description}</TableCell>
                                <TableCell align="center" ><img src={pokemon.image} className={classes.Media}></img></TableCell>
                            </TableRow>
                        }
                        {/* Maps back the table for a specific pokemon */}
                    </TableBody>
                </Table>
            </TableContainer>
            <Button variant="contained" color="secondary" onClick={() => listView()}>Return</Button>
            {/* Returns user to the whole list */}
        </div>
    );
}

export default View;
