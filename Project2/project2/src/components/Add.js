import React, { useEffect, useState } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
    Button: {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(2)
    }
}));

/**
 * Allows the addition of a new pokemon to the database
 * @param {setCurrentComponent} props 
 * @returns A new pokemon to the table, unless canceled 
 */
function Add(props) {
    const [pokemonName, setPokemonName] = useState("");
    const [pokemonType, setPokemonType] = useState("");
    const [pokemonDescription, setPokemonDescription] = useState("");
    const [pokemonImage, setPokemonImage] = useState("");
    const [buttonDisabled, setButtonDisabled] = useState(true); //Button enabling and disabling, starts disables
    const classes = useStyles();

    const pokemonUri = "https://web-app-pokemon.herokuapp.com"; //Base pokemon Uri link

    /**
     * Will change component to list view when called
     */
    function ListView() {
        props.setCurrentComponent("List");
    }

    //Enables button if all inputs are full, otherwise disabled
    function validateInput() {
        if (pokemonName && pokemonType && pokemonDescription && pokemonImage) {
            setButtonDisabled(false);
        } else {
            setButtonDisabled(true);
        }
    }

    /**
     * Called when adding a pokemon, will add the pokemon based on inputs from users and change view back to list
     */
    async function addPokemon() {
        try {
            const response = await axios({
                method: "post", //you can set what request you want to be: delete, get, post
                url: `${pokemonUri}/pokemon`,
                data: { name: pokemonName, type: pokemonType, description: pokemonDescription, image: pokemonImage },
                headers: {
                    "User-Id": "Jacob-R",
                    "Content-Type": "application/json"
                }
            });
            //return to the list view
            props.setCurrentComponent("List");
        } catch (e) {
            //some error occurred
        }
    }

    useEffect(() => {
        validateInput();
    });

    return (
        <div>
            <div>
                <Button variant="contained" color="primary" onClick={() => ListView()} className={classes.Button}>
                    Cancel?
                </Button>
            </div>
            <TextField
                id="PokemonName"
                variant="outlined"
                helperText="Pokemon Name"
                onChange={(event) => setPokemonName(event.target.value)}
            ></TextField>
            <TextField
                id="PokemonType"
                variant="outlined"
                helperText="Pokemon Type"
                onChange={(event) => setPokemonType(event.target.value)}
            ></TextField>
            <TextField
                id="PokemonDescription"
                variant="outlined"
                helperText="Pokemon Description"
                onChange={(event) => setPokemonDescription(event.target.value)}
            ></TextField>
            <TextField
                id="PokemonImage"
                variant="outlined"
                helperText="Pokemon Image Link"
                onChange={(event) => setPokemonImage(event.target.value)}
            ></TextField>
            <div>
                <Button
                    disabled={buttonDisabled}
                    variant="contained"
                    color="secondary"
                    onClick={() => addPokemon()}
                    className={classes.Button}
                >
                    Add Pokemon?
                </Button>
            </div>
        </div>
    );
}

export default Add;
