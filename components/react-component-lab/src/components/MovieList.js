import React, { useState } from "react";


/**
 * A movie list is created and shown to the user
 * @returns a movie list array that is displayed through divs
 */
function movieList() {
    const list = ["Star Wars", "Godfather", "Shrek 2", "Good Fellas"];

    return (
        <div>
            <div>{list[0]}</div>
            <div>{list[1]}</div>
            <div>{list[2]}</div>
            <div>{list[3]}</div>
        </div>
    );
}

export default movieList;