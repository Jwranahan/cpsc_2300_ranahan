import React, { useState } from "react";


/**
 * This will allow 3 words to be entered that will be put in the story
 * @returns The mad lib components are returned to the main story
 */
function MadLib() {
    const [madLib1, setMadLib1] = useState("");
    const [madLib2, setMadLib2] = useState("");
    const [madLib3, setMadLib3] = useState("");

    return (
        <div>
            <div>
                <input onChange={(e) => setMadLib1(e.target.value)} placeholder="Enter a Noun"></input>
                <input onChange={(e) => setMadLib2(e.target.value)} placeholder="Enter a Verb"></input>
                <input onChange={(e) => setMadLib3(e.target.value)} placeholder="Enter an Adjective"></input>
            </div>
            <div>
                <h1>
                    There once was a {madLib1} that lived on a farm. They were very {madLib3} about the locals living
                    around them, so they {madLib2} to the locals to tell them their true feelings about the economy.
                </h1>
            </div>
        </div>
    );
}

export default MadLib;
