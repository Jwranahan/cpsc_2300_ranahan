import "./App.css";
import React, { useState } from "react";
import MovieList from "./components/MovieList";
import MadLib from "./components/MadLib";

/**
 * If a checkbox is checked, the respective component is pulled up
 * @returns Will display 2 checkboxes that will display seperate components when checked
 */
function App() {
    const [showMovies, setShowMovies] = useState(false);
    const [madLib, setMadLib] = useState(false);

    return (
        <div className="App">
            <span>Show Movies?</span>
            <input type="checkbox" checked={showMovies} onChange={(e) => setShowMovies(e.target.checked)}></input>
            {showMovies && (
                <React.Fragment>
                    <MovieList />
                </React.Fragment>
            )}
            <div>
                <span>Show MadLib?</span>
                <input type="checkbox" checked={madLib} onChange={(e) => setMadLib(e.target.checked)}></input>
                {madLib && (
                    <React.Fragment>
                        <MadLib />
                    </React.Fragment>
                )}
            </div>
        </div>
    );
}

export default App;
