import "./App.css";
import React, { useState } from "react";
import List from "./components/List";
import View from "./components/View";
import Add from "./components/Add";
import logo from "./images/PokemonLogo.jpg";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    Media: {
        maxHeight: 225,
        width: "auto"
    }
}));

function App() {
    const classes = useStyles();
    const [currentComponent, setCurrentComponent] = useState("List"); //Start app view on list view
    const [currentPokemon, setCurrentPokemon] = useState(""); //Pokemon currently being viewed

    return (
        <div className="App">
            <img className={classes.Media} src={logo} alt="pokeball" />
            {currentComponent === "List" && (
                <List setCurrentComponent={setCurrentComponent} setCurrentPokemon={setCurrentPokemon} />
            )}
            {currentComponent === "View" && (
                <View setCurrentComponent={setCurrentComponent} currentPokemon={currentPokemon} />
            )}
            {currentComponent === "Add" && <Add setCurrentComponent={setCurrentComponent} />}
        </div>
    );
}

export default App;
