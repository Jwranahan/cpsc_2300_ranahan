import React, { useEffect, useState } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles((theme) => ({
    tableStyle: {
        marginLeft: "auto",
        marginRight: "auto",
        width: "80%"
    },
    Button: {
        marginBottom: theme.spacing(3)
    },
    IconButton1: {
        color: theme.palette.primary.main
    },
    IconButton2: {
        color: theme.palette.secondary.main
    }
}));

/**
 * Allows for full view of all pokemon through a table. Will also allow change of current view state
 * @param {setCurrentComponent, setCurrentPokemon} props 
 * @returns A list of all current pokemon, will pass back a value through setCurrentComponent upon request
 */
function List(props) {
    const [pokemon, setPokemon] = useState([]);
    const classes = useStyles();

    const pokemonUri = "https://web-app-pokemon.herokuapp.com";

    //Will load up the website and get the pokemon through the useEffect
    async function loadPokemonList() {
        try {
            const response = await axios({
                method: "get", //will get the list of all current pokemon
                url: `${pokemonUri}/pokemon`,
                headers: {
                    "User-Id": "Jacob-R"
                }
            });
            //set the pokemon list to retrieved data on success
            setPokemon(response.data);
        } catch (e) {
            //some error occurred
        }
    }

    useEffect(() => {
        loadPokemonList();
    }, []);

    async function deletePokemon(id) {
        try {
            console.log(id);
            const response = await axios({
                method: "delete", //will get the list of all current pokemon
                url: `${pokemonUri}/pokemon/${id}`, //Remove pokemon based on ID passed
                headers: {
                    "User-Id": "Jacob-R"
                }
            });
            loadPokemonList(); //Rerun the loading to get new list
        } catch (e) {
            //some error occurred
        }
    }

    function viewSetup(id) {
        props.setCurrentPokemon(id);
        props.setCurrentComponent("View");
    }

    function addPokemon() {
        props.setCurrentComponent("Add");
    }

    //Returns the table of all pokemon on load
    return (
        <div>
            <Button variant="contained" color="primary" onClick={() => addPokemon()} className={classes.Button}>
                Add Pokemon?
            </Button>
            <TableContainer className={classes.tableStyle}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Pokemon</TableCell>
                            <TableCell align="center">Type</TableCell>
                            <TableCell align="center">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {pokemon.map((pokemonResult) => (
                            <TableRow key={pokemonResult.id}>
                                <TableCell align="center">{pokemonResult.name}</TableCell>
                                <TableCell align="center">{pokemonResult.type}</TableCell>
                                <TableCell align="center">
                                    <IconButton
                                        className={classes.IconButton1}
                                        onClick={() => viewSetup(pokemonResult.id)}
                                        title="View Pokemon"
                                    >
                                        <VisibilityIcon />
                                    </IconButton>
                                    <IconButton
                                        className={classes.IconButton2}
                                        onClick={() => deletePokemon(pokemonResult.id)}
                                        title="Delete Pokemon"
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default List;
