"use strict";

// object - holds on to key/value pairs and functions
// acts like an instantiated class
// key value pairs -- acts like dictionary to python
// or hashmap in c#/java

// define with curly braces
// can't have any type
const person = {};
person.firstName = "Jacob";
person.lastName = "Ranahan";
person.fullName = function () {
    return `${this.firstName} ${this.lastName}`;
};

// You can use square brackets after your object with the
// field name in quotes to retrieve the value
console.log(person["lastName"]);

if (person.lastName) {
    console.log(person.lastName);
}

person.lastName += "a student";

// More verbose declarations uses a little more memory to
// use this keyword, but the amount difference is not consequential ever
const person3 = new Object();
person3.firstName = "Josh";
person3.lastName = "Huffman";
person3.fullName = function () {
    return `${this.firstName} ${this.lastName}`;
};

console.log(person);

console.log(`My name is ${person.firstName} ${person.lastName}`);
console.log(person.fullName());

// undefined - value was never set or field doesn't exist
// null - value is empty - but could have been set intentionally

const person2 = { firstName: "Chad", lastName: "Wallace" };
person2.title = "Dept. Chair";

console.log(person2);

/**
 * function constructor for object
 * PascalCase for this
 */
function Person(firstName, lastName) {
    // an underscore defines a field that should be treated
    // as private, even though it's not enforceable in js
    this._firstName = firstName;
    this._lastName = lastName;
    this.fullName = function () {
        return `${this._firstName} ${this._lastName}`;
    };
}

const frank = new Person("Frank", "Uzorh");
const gage = new Person("Gage", "Jones");
console.log(frank.fullName());
console.log(gage.fullName());
