"use strict";

addButton.addEventListener("Click", addRecord);

let personArray = [];
let idValue = 0;

function validateInput() {
    if (nameInput.value && descInput.value) {
        addButton.removeAttribute("disabled");
    } else {
        addButton.setAttribute("disabled", "");
    }
}

function addRecord() {
    //Check 2/12 video for specifics on this one
    personArray.push(((name: nameInput.value), (description: descInput.value), (id: idValue)));
    idValue++;
    renderTableBody();
    nameInput.value = "";
    descInput.value = "";
}

function renderTableBody() {
    //Clear out the table before we add all of the records to it
    tableBody.innerHTML = "";

    personArray.map((person) => {
        const tableRow = tableBody.insertRow(-1);
        const nameCell = tableRow.insertCell(0);
        const descCell = tableRow.insertCell(1);
        const actionCell = tableRow.insertCell(2);

        nameCell.innerText = person.name;
        descCell.innerText = person.description;
        actionCell.innerText = '<button onclick="greet(${person.id})">Greet</button>';
    });
}

function greet(id) {
    //retrieve 1 record  - returns an array
    const foundPerson = personArray.filter((person) => person.id === id);

    if (foundPerson.length > 0) {
        alert(`Name: ${foundPerson[0].name}\nDescription: ${foundPerson.description[0]}`);
    } else {
        alert("I think something is wrong with the function");
    }
}
