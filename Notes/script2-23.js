"use strict";

const person = {
    firstName: "Jacob",
    lastName: "Ranahan",
    fullName: function () {
        return `${this.firstName} ${this.lastName}`;
    }
};

//parse JSON parse in a try/catch block for more success
try {
    console.log(JSON.parse("Jacob"));
} catch (error) {
    console.log(error);
}

const personJson = JSON.stringify(person);
const jsonString = "('name': 'Jacob')";
let newObject;
try {
    //description json string to an object
    newObject = JSON.parse(personJson);
    console.log(JSON.parse(personJson));
} catch (error) {
    console.log(error);
}
