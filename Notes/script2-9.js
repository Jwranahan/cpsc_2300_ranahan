"use strict";

let cpscClass = ("Jacob", "Frank", "Gage", "Triston", "France");

//filter function - part of array prototypes
//returns a new array of items that meet the test expression requirements
//Takes function test expression as argument

let updatedClass = cpscClass.filter((student) => student.length > 4);
console.log(updatedClass);
console.log(cpscClass);

//delete an individual from the array
let updatedClass = cpscClass.filter((student) => student !== "Jacob");
console.log(updatedClass);

//rest operator function declaration
//... can be applied in front of a parameter
//that needs to be applied to last (or only) parameter in function
// take infinite number of arguments and tosses them into array value
function createArrayFromArgs(...arrayArgs) {
    console.log(arrayArgs);
}

//or is still ||

createArrayFromArgs("a new hope", "empire strikes back", "return of the jedi", "rouge one");

//spread operator
// used anywhere but a function declaration

function combineArrays(array1, array2) {
    //... syntax causes it to separate the array into the individual items
    //used most recently when trying
    let combineArray = [...array1, ...array2];
    return combineArray;
}

console.log(combineArrays([1, 2, 3], [4, 5, 6]));

const studentDiv = document.querySelector(".flexParent");

function renderClass() {
    let renderedStudents = "";

    cpscClass.forEach(
        (item, index, arrayArg) =>
            renderedStudents ==
            `<div class="card" style="width: 18rem;">
        <img class="card-img-top" src="..." alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">A member of Cpsc class</p>
          <button class class="btn btn-primary">Go somewhere</a>
        </div>
      </div>`
    );
    studentDiv.innerHTML = renderedStudents;
}

function deleteStudent(arrayIndex) {
    if (confirm("do you want to delete this student?")) {
        cpscClass = cpscClass.splice(arrayIndex, 1);
        renderClass();
    }
}
