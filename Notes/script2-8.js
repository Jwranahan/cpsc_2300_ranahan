"use strict";

// // let myArray = [1, "string", ( objectName; "some name" )];
// // console.log(myArray);

// // let declaredArray = new Array(1, 2, 3, 4);

// const arrayExample = [];

// arrayExample.push("some string");
// arrayExample.push("some other string");

// console.log(arrayExample);

// //arrays are a zero based index
// console.log(arrayExample[1]);

// //array has a length property
// console.log(arrayExample.length);


let cpscClass = ("Jacob", "Frank", "Gage", "Triston", "France");
//foreach() - Comes with array prototype
//3 arguements
//1st - value of the current item
//2nd - index of the current item
//3rd - actual array that you're interacting with (for modification purposes)

// cpscClass.forEach()
// //Needs fixing
// function outputClass(item, index, actualArray) {
//     console.log('Item: $(item)\n' )
// }

// cspsClass.forEach([item, index, actualArray =>] console.log('item: '))

//Array splice method
//Takes 3 arguements
//1st - Starting index
//2nd - How many items to remove/delete
//3rd+ - New items to include at index
//Returns the removed object

const removedPerson = cpscClass.splice(5, 1, "Garth", "Brooks");
console.log(removedPerson);

console.log(cpscClass);
