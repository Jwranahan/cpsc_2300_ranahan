"use strict";

function setup() {
    createCanvas(1200, 400);
}

function draw() {
    background(214, 286, 104);
    for (x = 1; x < 26; x++) {
        if (x / 3) {
            if (x / 3 && x / 5) {
                square(x * 25, 200, 15);
                fill("A600ff");
            } else {
                circle(x * 25, 200, 15);
                fill("A89403");
            }
        }
        if (x / 5) {
            circle(x * 25, 200, 15);
            fill("A00000");
        } else {
            circle(x * 25, 200, 15);
            fill("BB4570");
        }
    }
}
