"use strict";

let letterGrade = ""; //The final letter grade is stored here (A, B, C, D, F)
let finalGrade = 0; //The final grade in number form
let pointsEarned = 0; //Total points earned
let totalPoints = 0; //Total points possible

/**
 * Toggle the first page to visible and turn the second invisble
 */
function togglePage1() {
    document.getElementById("page2").style.display = "none";
    document.getElementById("page1").style.display = "block";
}

/**
 * Toggle the second page to visible and turn the first invisible
 */
function togglePage2() {
    document.getElementById("page1").style.display = "none";
    document.getElementById("page2").style.display = "block";
}

/**
 * Will check to see if all three input boxes are filled, then will pass inputs to addAssignment
 * 3 total inputs, one from each field on the second page
 */
function checkForValid() {
    let input1 = document.getElementById("assignName").value;
    let input2 = document.getElementById("points").value;
    let input3 = document.getElementById("tPoints").value;
    if (input1 == "" || input2 == "" || input3 == "") {
        //If one or more of the inputs are blank, will alert the user and stop
        alert("One or more input fields are blank, please fill in all fields");
    } else {
        addAssignment(input1, input2, input3);
    }
}

/**
 * Will take the 3 inputs and add them to the table, while also adding them to the final grade
 * @param {*} input1 - The assignment name
 * @param {*} input2 - The points earned on that assignment
 * @param {*} input3 - The total possible points
 *         ^ I really like the color of this
 */
function addAssignment(input1, input2, input3) {
    let table = document.getElementById("myTable");
    let row = table.insertRow(1);
    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    let cell3 = row.insertCell(2);
    let cell4 = row.insertCell(3); //Prepares a blank row for the inputs
    const newInput2 = Number(input2);
    const newInput3 = Number(input3); //converts the points earned and total points to numbers
    pointsEarned = pointsEarned + newInput2;
    totalPoints = totalPoints + newInput3; //Adds the converted values to the global variables
    calcAvg(); //Calculates the average
    cell1.innerHTML = input1;
    cell2.innerHTML = input2;
    cell3.innerHTML = input3;
    cell4.innerHTML = "<button onclick='deleteRow(this)'></i></button>";
    //class='btn btn-outline-danger''><i class='fas fa-minus-circle'></i> Code for fontawesome
    //All 4 cells are being added ^^^
}

/**
 * Is going to delete row specific and update the final grade accordingly
 * @param {*} row Uses parameter this in button to pass exact row id
 */

function deleteRow(row) {
    let gone = confirm("Are you sure you want to delete this?"); //This will ask the user to confirm deletion of the row
    if (gone == true) {
        let i = row.parentNode.parentNode.rowIndex;
        let tempP = document.getElementById("myTable").rows[i].cells[1].innerHTML; //grabs the points earned from row being deleted
        let tempTP = document.getElementById("myTable").rows[i].cells[2].innerHTML; //grabs the total points from row being deleted
        let convertedP = Number(tempP);
        let convertedTP = Number(tempTP); //Converts both numbers to actual numbers
        pointsEarned = pointsEarned - convertedP;
        totalPoints = totalPoints - convertedTP; //Updates total points and points earned
        if (totalPoints > 0) {
            //If there are total points left, it will recalculate the final grade
            calcAvg();
        } else {
            //Otherwise, if defaults back to the original value
            document.getElementById("Grade").innerHTML = "Final Grade";
        }
        //The deletion is finally done here
        document.getElementById("myTable").deleteRow(i);
    } else {
    }
}

/**
 * Will take the pointsEarned and totalPoints and divide them to get the final grade
 * After getting the final grade, it will round it up and run letter() to get the letter grade
 */
function calcAvg() {
    finalGrade = 100 * (pointsEarned / totalPoints);
    finalGrade = Math.round(finalGrade);
    letter();
}

/**
 * Will take the final grade and compare it to normal letter grade value to return
 * one of five letters for the html code, which is put together at the end
 */
function letter() {
    //Checks each option starting at an A
    if (finalGrade >= 90) {
        letterGrade = "A";
    }
    if (finalGrade >= 80 && finalGrade < 90) {
        letterGrade = "B";
    }
    if (finalGrade >= 70 && finalGrade < 80) {
        letterGrade = "C";
    }
    if (finalGrade >= 60 && finalGrade < 70) {
        letterGrade = "D";
    }
    if (finalGrade < 50) {
        letterGrade = "F";
    }
    //Changes the HTML text on the home page to be the new final grade with the letter grade
    document.getElementById("Grade").innerHTML = finalGrade + "% " + letterGrade;
}

//Notes for storage on 2/23

//run on page load and attempts to retirive data from local storage
//Then sets the assignment array to the found data (if present)
// function getDataFromStorage() {
//     let storageData = window.localStorage.getItem("Grade");
//     if (storageData) {
//         try {
//             //assignmentArray = JSON.parse(storageData);
//         } catch(e) {
//             console.log(e);
//             //assignmentArray = {};
//         }
//     }
//     renderTableBody();
// }

// function setStorage(assignment) {
//     try {
//         window.localStorage.setItem("Grade", JSON.stringify(assignment));
//     } catch(e) {
//         console.log(e);
//     }
// }
